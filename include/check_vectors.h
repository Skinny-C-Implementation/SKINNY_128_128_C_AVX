#pragma once

extern int skinny128_64_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);

int check_test_vector(int blocks_number);
