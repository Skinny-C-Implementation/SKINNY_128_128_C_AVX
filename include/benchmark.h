extern int skinny128_64_blocks(unsigned char *output,
		       unsigned char *input,
		       unsigned long long input_length,
		       const unsigned char *key);

int cmp_dbl(const void *x, const void *y);
void benchmark_cipher(int blocks_number);
void get_random_imput(unsigned char *input, unsigned char *key, unsigned long long input_length);
