#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "avx_declaration.h"
#include "timing.h"
#include "skinny128.h"
#include "benchmark.h"
#include "check_vectors.h"

int main(int argc, char *argv[]){
  printf("Skinny 128-128 benchmarking starting\n");
  //Check if the test vector is OK
  if(check_test_vector(64)){
    benchmark_cipher(64);
  }
  else{
    printf("ERROR: Unable to process to the benchmarking. Tests are not ok. \n");
  }
  printf("End\n");
}
