#include <stdio.h>
#include <string.h>

#include "avx_declaration.h"
#include "check_vectors.h"
#include "test_vectors.h"

int check_test_vector(int blocks_number){
  // Variables declaration
  unsigned long long input_length = blocks_number*16;
  unsigned char *input = malloc(input_length*sizeof(u8));
  unsigned char *output  = malloc(input_length*sizeof(u8));
  
  // Test vector
  unsigned char* plaintext;
  unsigned char* key;
  unsigned char* ciphertext;
  
  key = key_test;
  plaintext = plaintext_64;
  ciphertext = ciphertext_64;

  // Duplication of the test vector
  for(int i=0; i<blocks_number; i++){
    memcpy(input, plaintext, blocks_number*16);
  }
  
  // Encryption of the test vector
  skinny128_64_blocks(output, input, input_length, key);
  
  // Validation
  for(int i=0; i<(blocks_number*16); i++) {
    if(output[i] != ciphertext[i]) {
      printf("ERROR: Outputstream does not match test vector at position %i!\n", i);
      free(input);
      free(output);
      return 0;
    }
  }
  
  free(input);
  free(output);
  return 1;
}
