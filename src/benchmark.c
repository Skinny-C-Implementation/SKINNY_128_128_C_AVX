#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "avx_declaration.h"
#include "timing.h"
#include "benchmark.h"

#define NUM_TIMINGS 2000

int cmp_dbl(const void *x, const void *y)
{
  double xx = *(double*)x;
  double yy = *(double*)y;
  
  if(xx < yy){
    return -1;
  }
  
  if(xx > yy){
    return  1;
  }
  
  return 0;
}

void benchmark_cipher(int blocks_number){
  unsigned long long input_length = blocks_number*16*NUM_TIMINGS;
  unsigned char *input = (unsigned char*) malloc(input_length*sizeof(u8));
  unsigned char *output  = (unsigned char*) malloc(input_length*sizeof(u8));
  unsigned char *key  = (unsigned char*) malloc(16*sizeof(u8));
  u64 timer = 0;
  double timings[NUM_TIMINGS];

    //Get random inputs
  for(int i =-100; i<NUM_TIMINGS; i++){
      get_random_imput(input, key, input_length);
    
      //Benchmark the encryption
      timer = start_rdtsc();
      skinny128_64_blocks(output, input, input_length, key);
      timer = end_rdtsc() - timer;
     
      //Fill the array with all the timings values
      if(i>=0 && i<NUM_TIMINGS){
	timings[i] = ((double)timer) / input_length;
      }
  }
  //Get the median
  qsort(timings, NUM_TIMINGS, sizeof(double), cmp_dbl);
  printf("Skinny-128-128 %d blocks: %f cycles per byte\n", blocks_number, timings[NUM_TIMINGS / 2]);
  
  free(input);
  free(output);
  free(key);
}

void get_random_imput(unsigned char *input, unsigned char *key, unsigned long long input_length){
  
  for(int j=0; j<input_length; j++){
    input[j] = rand() & 0xff;
  }
  for(int j=0; j<16; j++){
    key[j] = rand() & 0xff;
  } 
}
