SRCDIR=src
LIBDIR=lib
BINDIR=bin
INCLUDEDIR=include
CC=gcc
AR=ar
CFLAGS=-Wall -pedantic -g -std=gnu99 -O3 -I$(INCLUDEDIR) -I$(HOME)/local/include -mavx2
LDFLAGS=-L$(LIBDIR) -lcipher -lbenchmark
EXEC=Skinny_128_128_AVX
CREATE=mkdir

const :
	@ if ! [ -d $(BINDIR) ]; then \
		echo "Création du dossier $(BINDIR)"; \
		$(CREATE) $(BINDIR); \
	fi
	@ if ! [ -d $(LIBDIR) ]; then \
		echo "Création du dossier $(LIBDIR)"; \
		 $(CREATE) $(LIBDIR); \
	fi

all : const $(BINDIR)/$(EXEC)

$(SRCDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

$(LIBDIR)/libbenchmark.a : $(SRCDIR)/check_vectors.o $(SRCDIR)/timing.o $(SRCDIR)/benchmark.o
	$(AR) -r $@ $^

$(LIBDIR)/libcipher.a : $(SRCDIR)/skinny128.o $(SRCDIR)/packing.o
	$(AR) -r $@ $^

$(BINDIR)/$(EXEC) : $(SRCDIR)/main.o $(LIBDIR)/libcipher.a $(LIBDIR)/libbenchmark.a
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)


clean :
	rm -rf ./$(BINDIR)/*
	rm -rf ./$(LIBDIR)/*.a
	rm -rf ./$(SRCDIR)/*.o
	rm -rf ./$(SRCDIR)/*.c~
	rm -rf ./$(INCLUDEDIR)/*.h~
